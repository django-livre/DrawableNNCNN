/**
	Função de treinamento do modelo
	@returns {tf.Model}
**/
async function trainAndTest(dataTrain, dataTest, model, epochs, batchSize) {
	let validationSplit = 0.15;

	await model.fit(dataTrain.data, dataTrain.label , {
		epochs,
		batchSize,
		validationSplit
	});

	const classifierOutput = model.evaluate(dataTest.data, dataTest.label);
  	console.log(
      `\nResultado:\n` +
      `Loss = ${classifierOutput[0].dataSync()[0].toFixed(3)}; `+
      `Accuracy = ${classifierOutput[1].dataSync()[0].toFixed(3)}`);

  	return model;
}

module.exports = {
	trainAndTest
};
