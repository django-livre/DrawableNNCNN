'''
Script para converter o formato Keras (.h5) para tflite
'''

import tensorflow as tf

# Carrega o modelo no formado Keras
converter = tf.contrib.lite.TocoConverter.from_keras_model_file('../drawablemodel/model_keras.h5')
tflite_model = converter.convert() # Converte
open('../drawablemodel/model_tflite.tflite', 'wb').write(tflite_model) # Salva a conversão
