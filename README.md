## DrawableNN - Convolutional Neural Network :zap:

Rede neural utilizada no aplicativo DrawableNN.

### Executando :dragon:

> Cuidado guerreiro é aqui que vivem os dragões ! :dragon_face: 
> A execução deste código pode consumir muitos recursos de sua máquina! Certifique-se de ter uma máquina boa!

Para realizar o treinamento da rede neural em sua máquina será necessário ter instalado:

- NodeJS >= 8.11;
- NPM

Com estas dependências instaladas execute dentro do diretório do projeto:

```sh
npm install # Instala as dependências
```

```sh
node src/cnn.js
```

## Obtendo os dados :elephant:

Deep Learning depende muito da quantidade de dados para seu sucesso! Por isto a base de dados do Google `Quick Draw` foi utilizada para a realização do processo de treinamento da rede. 

Os dados são adquiridos através da biblioteca `quickdraw.js`, para realizar este processo é necessário executar o script `data/download_data.js`, para isto utilize o comando.

```sh
node data/download_data_complete.jss
```

`OBS`: Após diversos testes, percebeu-se que, a biblioteca `quickdraw.js` utilizada para a rápida aquisição dos dados funciona sem problemas no Windows, porém apresenta alguns problemas na execução no Linux, mas funciona, você só terá alguns problemas. Porém, há um arquivo nomeado `quickdraw.js.zip`, dentro do diretório `data`, caso você queira, descompacte ele e substitua o conteúdo do diretório `quickdraw.js` dentro do `node_modules`, pelo conteúdo descompactado :smile:

## Conversão do model

Após o treinamento da rede, pesos são gerados, estes podem ser convertidos para outros formatos, e desta forma, o modelo treinado pode ser utilizado em qualquer lugar, para realizar isto utilize o script `converter.sh` 

Este script primeiro transforma o modelo do tfjs em h5 (Keras), e em seguida gera o arquivo tflite, que poderá ser utilizado no android.

```shell
./converter.sh
```

Todos os resultados são salvos no diretório `drawablemodel`.

## Dockerfile :whale:

Caso você tenha problemas com a instalação do Tensorflow.js é possível utilizar uma imagem `Docker` para fazer isto. Para fazer `build` utilize o comando abaixo

```sh
docker build -t drawablenn/environment .
```
