/**
	Script para baixar todos os dados do Google Quick Draw para o
	treinamento completo da rede.
**/

(async function() {
	const quickDraw = require('quickdraw.js');
	console.log('Iniciando processo de aquisição dos dados')

	// Importando 7000 dados de todas as categorias (Imagens com dimensões 28x28)
	await quickDraw.importAll(7000, 28);
})();
