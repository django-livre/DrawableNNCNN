const tf = require('@tensorflow/tfjs');
require('@tensorflow/tfjs-node');

/**
	Função utilizada para criar um modelo de rede neural convolucional
	@returns {tf.Model}
**/
function createConvolutionalModel(imageHeigth, imageWeight, classNumber) {
	const model = tf.sequential();
	const optimizer = 'rmsprop';
	const lossMethod = 'categoricalCrossentropy';
	const metrics = ['accuracy'];

	// Adicionando camada convolucional
	model.add(tf.layers.conv2d({
		inputShape: [imageHeigth, imageWeight, 1],
		kernelSize: 3,
		filters: 32,
		activation: 'relu'
	}));

	// Adicionando camada convolucional
	model.add(tf.layers.conv2d({
  		filters: 32,
  		kernelSize: 3,
  		activation: 'relu',
	}));

	/*
		Após uma camada (Neste caso, conjunto) convolucional utiliza-se uma camada de polling
		para diminuir a quantidade de dados processados (LeCun, 1998)
	*/
	model.add(tf.layers.maxPooling2d({
		poolSize: [2, 2]
	}));

	model.add(tf.layers.conv2d({
	  filters: 64,
	  kernelSize: 3,
	  activation: 'relu',
	}));

	model.add(tf.layers.conv2d({
	  filters: 64,
	  kernelSize: 3,
	  activation: 'relu',
	}));

	model.add(tf.layers.maxPooling2d({poolSize: [2, 2]}));

	// Diminuindo a dimensionalidade dos dados
	model.add(tf.layers.flatten());

	// Camada experimental (Ainda estou lendo artigos sobre ela)
	model.add(tf.layers.dropout({rate: 0.25}));

	// Camada FC para iniciar a classificação
	model.add(tf.layers.dense({units: 512, activation: 'relu'}));

	// Camada experimental (Ainda estou lendo artigos sobre ela)
	model.add(tf.layers.dropout({rate: 0.5}));

	// Classifica os dados se baseando no número de classes inseridos pelo usuário
	model.add(tf.layers.dense({units: classNumber, activation: 'softmax'}));

	// Compilando toda a rede criada em um modelo, e ainda, adicionando
	// um mecanismo de otimização
	model.compile({
		optimizer: optimizer,
		loss: lossMethod,
		metrics: metrics
	});

	return model;
}

async function saveModel(model, pathToSave) {
	await model.save(pathToSave);
	console.log('Modelo salvo com suceso!');
}

module.exports = {
	saveModel, createConvolutionalModel
}
