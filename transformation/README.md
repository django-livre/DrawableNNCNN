## Conversão do modelo :crystal_ball:

Caso você esteja treinando novamente a rede, será necessário converter o modelo do `tfjs` para `tflite`. Para isto utilize o script `converter.sh`. Porém antes de sua execução, realize a instalação das dependências.

```shell
pipenv install 
```

> Caso você não tenha o pipenv instalado basta utilizar o comando `pip install pipenv`.

Após a instalação execute o arquivo `converter.sh`.

```shell
./converter.sh
```

> Veja que, o comando executado no script pode mudar de acordo com a configuração de sua distribuição.
