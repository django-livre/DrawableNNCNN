FROM debian
MAINTAINER Felipe Menino
RUN apt-get update -y && wget -qO- https://deb.nodesource.com/setup_8.x | -E bash - && sudo apt-get install -y nodejs && apt-get install -y build-essential && apt-get install git
CMD /bin/bash