(async function() {

const modelUtil = require('./model');
const trainUtil = require('./train');
const dataUtil = require('./data');

const tf = require('@tensorflow/tfjs'); // Utiliza o TFJS
require('@tensorflow/tfjs-node'); // Indica que será rodado no NodeJS

// Configurações gerais
const epochs = 20; // Quantidade de 'iterações' executadas
const batchSize = 128; // Tamanho do bloco de exemplos inseridos por iteração no treinamento
const pathModel = 'file://drawablemodel';
const imageHeigth = 28;
const imageWeight = 28;

// Carregando os dados de treino e teste
let classes = ['airplane', 'knife', 'finger', 'scorpion', 
				'megaphone', 'panda', 'clock', 'donut', 'stereo', 'sun'];
let dataTrain = dataUtil.loadSomeClassesData(imageHeigth, imageWeight, classes, 60000);
let dataTest = dataUtil.loadSomeClassesData(imageHeigth, imageWeight, classes, 12000); // 20% do total

// Criando modelo CNN (Para imagens 28 x 28, e N classes)
const modelCNN = modelUtil.createConvolutionalModel(imageHeigth, imageWeight, classes.length);

// Treinando a rede e testando
const trainedModelCNN = await trainUtil.trainAndTest(dataTrain, dataTest, modelCNN, epochs, batchSize);

// Salvando o modelo
modelUtil.saveModel(trainedModelCNN, pathModel);
})();
