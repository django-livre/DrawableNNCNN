#!/bin/bash
# Script para realizar a conversão do modelo gerado para um modelo Keras

# Realiza a conversão do modelo TFJS para Keras
tensorflowjs_converter \
	--input_format tensorflowjs --output_format keras \
	../drawablemodel/model.json ../drawablemodel/model_keras.h5

python3 keras_to_tflite.py