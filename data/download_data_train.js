/**
	Script para baixar todos os dados do Google Quick Draw para o
	treinamento de desenvolvimento da rede
**/

(async function() {
	const quickDraw = require('quickdraw.js');
	console.log('Iniciando processo de aquisição dos dados');
	let categories = ['airplane', 'knife', 'finger', 'scorpion', 
				'megaphone', 'panda', 'clock', 'donut', 'stereo', 'sun'];

	for (let i = 0; i < categories.length; i++) {
		let category = categories[i];
		console.log('Baixando ', category);
		await quickDraw.import(category, 100000);
	}
})();