const quickDraw = require('quickdraw.js');
const math = require('mathjs');
const tf = require('@tensorflow/tfjs');

/**
    Função que realiza o carregamento e rotulação dos dados
    @Returns{Object}
**/
function loadSomeClassesData(imageHeigth, imageWeight, classes, samples) {
    let imageShape = [samples, imageHeigth, imageWeight, 1];
    let data = new Float32Array(tf.util.sizeFromShape(imageShape));
    let label = new Int32Array(tf.util.sizeFromShape([samples, 1]));
    let set = quickDraw.set(samples, classes);

    let imageOffset = 0;
    let labelOffset = 0;
    for (let j = 0; j < samples; j++) {
        data.set(set.set[j].input, imageOffset);
        label.set(new Int32Array([set.set[j].output.indexOf(1)]), labelOffset);

        imageOffset += (imageHeigth * imageWeight);
        labelOffset += 1;
    }

    data = tf.tensor4d(data, imageShape);
    label = tf.oneHot(tf.tensor1d(label, 'int32'), classes.length).toFloat();

    return {
      'data': data,
      'label': label
    };
}

module.exports = {
    loadSomeClassesData
}
